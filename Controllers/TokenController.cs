﻿using DailyTask12_Authhentication.model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DailyTask12_Authhentication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [Route("GenerateToken")]
        public IActionResult Login([FromBody] userLogin userLogin)
        {
            var user = Authentication(userLogin);

            if(user != null)
            {
                var token = generate(user);
                return Ok(token);
            }
            return NotFound("User Not Found");
        }

        [HttpGet]
        [Authorize]
        [Route("ValidateToken")]
        public IActionResult ValidateToken()
        {
            return Ok("Congrats, your Token is Right");
        }

        private string generate(userModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:key"]));
            var credentialKey = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.Username),
                new Claim(ClaimTypes.Email,user.Email),
                new Claim(ClaimTypes.GivenName,user.Givenname),
                new Claim(ClaimTypes.Surname,user.Surname),
                new Claim(ClaimTypes.Role,user.Role),
            };
            var Token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires:DateTime.Now.AddMinutes(20),
                signingCredentials: credentialKey
                );

            return new JwtSecurityTokenHandler().WriteToken(Token);
        }

        private userModel Authentication(userLogin userLogin)
        {
            var currentUser = dataUser.Users.FirstOrDefault(o => o.Username.ToLower() == userLogin.Username.ToLower() && o.Password == userLogin.Password);

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }
    }
}
