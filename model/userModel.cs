﻿namespace DailyTask12_Authhentication.model
{
    public class userModel
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; }
        public string? Surname { get; set; }
        public string? Givenname { get; set; }

    }
}
